const express = require('express');

const { body, validationResult } = require('express-validator');
const { Compute } = require('../compute');

const router = express.Router();

const Operation = require('../models/Operation');

router.get('/', async (req, res) => {
  const resultBodyContent = {
    operations: await getOperationsList(),
    moment: require('moment')
  }
  res.render('main-content', resultBodyContent);
});

router.post('/', [
  body('a').isInt().withMessage('"a" must be a number'),
  body('b').isInt().withMessage('"b" must be a number'),
  body('operator').isIn(["+", "-", "x"]).withMessage('Operator must be "+", "-" or "x"'),
], async (req, res) => {

  // preprare return body
  const resultBodyContent = {
    moment: require('moment'),
    errors: []
  }

  const errors = validationResult(req);
  if (errors.isEmpty()) {
    const a = req.body.a;
    const b = req.body.b;
    const operator = req.body.operator;

    const compute = new Compute();

    var result = 0;
    switch (operator) {
      case "+":
        result = compute.add(a, b);
        break;
      case "-":
        result = compute.sub(a, b);
        break;    
      default:
        break;
    }

    const operation = new Operation({
      a: a,
      b: b,
      operator: operator,
      result: result
    })

    resultBodyContent.result = a + " " + operator + " " + b + " = " + result;

    await operation.save()
    .then(() => {})
    .catch(() => { 
      resultBodyContent.errors.push("Can't save data in database!");
    });
  } else {
    resultBodyContent.errors.push(...errors.array());
    resultBodyContent.data = req.body;
  }

  resultBodyContent.operations = await getOperationsList();
  res.render('main-content', resultBodyContent);
});

async function getOperationsList() {
  return await Operation.find().sort({"created_at": -1});
}

module.exports = router;