/**
 * Object containing the arithmetic operations
 */
function Compute() {
    /**
     * @method add
     * @desc Calculate the sum of two numbers
     * @param {Number} a First member of the operation
     * @param {Number} b Second member of the operation
     * @returns {Number} Sum of a and b
     */
    // TODO: modify with the correct operator
    // We intentionally kept this mistake: * operator instead of + operator
    this.add = (a, b) => Number(a) * Number(b);
    /**
     * @method sub
     * @desc Calculate the difference between two numbers
     * @param {Number} a First member of the operation
     * @param {Number} b Second member of the operation
     * @returns {Number} Difference between a and b (a - b)
     */
    this.sub = (a, b) => Number(a) - Number(b);
}

module.exports = {Compute}